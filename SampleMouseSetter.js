//  
//  マウス操作
//  
//  　マウスポインタの位置を設定。
//  

//  Shell関連の操作を提供するオブジェクトを取得
var sh = new ActiveXObject( "WScript.Shell" );

//  1秒処理を停止
WScript.Sleep( 1000 );

//  DLLファイルから関数を呼び出し、マウスポインタの位置を設定
//  (X座標=10, Y座標=10)
sh.Run( "rundll32.exe MouseEmulator.dll, _SetMouseXY@16 10, 10" );
WScript.Sleep( 1000 );

//  オブジェクトを解放
sh = null;